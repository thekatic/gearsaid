<?php
/**
 * @file
 * Returns the HTML for a block.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728246
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?> id="<?php print $block_html_id; ?>">

  <?php print render($title_prefix); ?>
    <input type="checkbox" href="#" class="menu-open" id="menu-open" name="menu-open">
    <label  for="menu-open" class="menu-open-button">
        <span class="hamburger hamburger-1"></span>
        <span class="hamburger hamburger-2"></span>
        <span class="hamburger hamburger-3"></span>
    </label>

  <?php print render($title_suffix); ?>

  <?php print $content; ?>

</div>
