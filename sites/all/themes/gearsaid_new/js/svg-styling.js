/**
 * Created by boban on 19.6.17..
 */

window.onload = function() {



    //BACKGROUND
    //svg background containers
    var singlePageWrapper = Snap("#single-page-overall-wrapper");

    //svg background behavior upon screen width change
    Snap.load("sites/all/themes/gearsaid_new/images-source/gearsaid-background-full.svg", function(data){
        singlePageWrapper.append(data);

        //svg background
        bgSVG = Snap(data.node);

        //viewport aspect ratio
        bgSVGWidth = parseInt(data.node.attributes[6].value);
        bgSVGAspectRatio = parseInt(data.node.attributes[6].value) / parseInt(data.node.attributes[7].value);
        //viewBox parameters
        viewBox = data.node.attributes[8].value;
        viewBoxParameters = viewBox.split(" ");

        viewBoxMinX =  Number(viewBoxParameters[0]);
        viewBoxMinY =  Number(viewBoxParameters[1]);
        viewBoxWidth = viewBoxParameters[2];
        viewBoxHeight = viewBoxParameters[3];

        bgSVGBehaviour(bgSVG, bgSVGWidth,  bgSVGAspectRatio, viewBoxMinX, viewBoxMinY);
        $(window).resize(function(){
            bgSVGBehaviour(bgSVG, bgSVGWidth, bgSVGAspectRatio, viewBoxMinX, viewBoxMinY);
        });
    });

    function bgSVGBehaviour(bgSVG, bgSVGWidth, bgSVGAspectRatio, viewBoxMinX, viewBoxMinY) {
        var initialWindowWidth = 1300; //from this breakpoint starts viewBox manipulation
        var currentWindowWidth = $(window).width();

        if(initialWindowWidth > currentWindowWidth) {
            var windowWidthChange =   (initialWindowWidth - currentWindowWidth)/currentWindowWidth;

            var newViewBoxMinX = viewBoxMinX*(1 + windowWidthChange) + (initialWindowWidth - currentWindowWidth)/2;
            var newViewBoxWidth = bgSVGWidth / (1 + windowWidthChange);
            var newViewBoxHeight = newViewBoxWidth / bgSVGAspectRatio;

            bgSVG.attr({
                viewBox: newViewBoxMinX+" "+viewBoxMinY+" "+newViewBoxWidth+" "+newViewBoxHeight
            });
        }
    }
    //end of BACKGROUND

    //HAMBURGER menu behavior on window scroll
    $(window).unbind('resize').bind('resize', function(){
        if($(window).width() > 1400){
            var vPos = $(window).scrollTop();
            hamburgerPosition(vPos);
            $(window).unbind('scroll').bind('scroll', function(){
                var vPos = $(window).scrollTop();
                hamburgerPosition(vPos);
            });
        } else {
            $(window).unbind('scroll');
            var vPos = 0;
            hamburgerPosition(vPos);

        }
    });

    function hamburgerPosition(vPos) {
        $("#block-menu-block-1").css("top", vPos);
    }
    //end of HAMBURGER

    //INTRO ANIMATION
    var introAnimationContainer = Snap("#svg-animation-image-container");

    Snap.load("sites/all/themes/gearsaid_new/images-source/intro-animacija.svg", function(data){
        introAnimationContainer.append(data);

        var introAnimation = Snap(data.node);
        introAnimation.attr({viewBox:"-80 0 1179.12 846.5"});
    });
    //end of INTRO ANIMATION

    //OFFICE ANIMATION
    var officeAnimationCOntainer = Snap("#svg-office-animation");
    Snap.load("sites/all/themes/gearsaid_new/images-source/office.svg", function(data){
        officeAnimationCOntainer.append(data);

        var officeoAnimation = Snap(data.node);

    });
    //end of OFFICE ANIMATION

}




