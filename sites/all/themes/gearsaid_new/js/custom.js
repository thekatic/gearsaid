
var $ = jQuery;

$(document).ready(function(){
    gooeyScroll();
});

//Gooey menu scroll to the desired section on single page site
function gooeyScroll() {
    var singlepageMenu = $('#block-system-main-menu');
    var singlepageMenuItems = $('#block-system-main-menu a');
    var gooeyMenu = $('#block-menu-block-1');
    var gooeyMenuItem = $('#block-menu-block-1 a');
    var destination = [];
    var i = 0;
    singlepageMenuItems.each(function(){
       destination.push($(this).attr('href'));
    });

    gooeyMenuItem.each(function(){
       $(this).attr('href', destination[i]);
       i++;
    });

    $('#block-menu-block-1 a').click(function(e){
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
    });

}