
var $ = jQuery;


console.log(jQuery().jquery);

jQuery(document).ready(function(){

	$(".single-page").addClass("layout-center");
	$("#block-system-main-menu").addClass("layout-center");

	
	/* Dodavanje .fade klase na navigation region */

	$(window).scroll(function(){
		var scrollPosition = $(window).scrollTop();

		if (scrollPosition > 0) {
			$(".region-navigation").addClass("fade");
		} else {
			$(".region-navigation").removeClass("fade");
		}

		/*if ($("#block-system-main-menu").hasClass("stickynav-active")){
			$("ul.menu").addClass("layout-center");
		} else {
			$("ul.menu").removeClass("layout-center");
		} */

		/* H1 fade out */
		var headingOpacity = scrollPosition/350;
		
		$(".heading").css("opacity", 1-headingOpacity);
		


	});

 


	/* Pokretanje masonry JS-a prilikom čekiranja opcija tehnologije na Portfolio view-u  */
  (function($) {
            Drupal.behaviors.myBehavior = {
                attach: function (context, settings) {
                    //code starts
                     $('.grid').masonry({
		   	  itemSelector: '.grid-item',
			  // use element for option
			  columnWidth: '.grid-item',
			  percentPosition: true

		  });

                }
            };
        })(jQuery);


     
     

 	 stickyNav();
     contactFormPadidng();
     languageSwitcher();
     navicon(); 

     $(window).resize(function(){
   		stickyNav();
   		navicon(); 
     	languageSwitcher();
     	contactFormPadidng();
     	
     });
     
     $(window).scroll(function(){
     	stickyNav();
     });


		/* vezano za navicon */   
		var windowWidth = $(window).width(); /* merim sirinu viewporta */  
		var tab = $(".region-navigation #block-system-main-menu h2"); 
		var card = $(".region-navigation"); /* zamenjujem parent element sa varijablom */
		var ulHeight = $(".region-navigation #block-system-main-menu ul").outerHeight(); /* merim visinu Ul elementa */
     	
     	$(tab).click(function(e){
     		e.stopPropagation();
	 		if($(this).hasClass("collapsed")) {
	 			
	 			$(this).addClass("expanded").removeClass("collapsed");
	 			$(card).css("bottom", 0);

	 			$(window).click(function(e){
	 				e.stopPropagation();
	 				if(event.target.nodeName == "LI" || event.target.nodeName == "A") return;
	 				
	 				$(card).css("bottom", -1*ulHeight);
	 				$(tab).addClass("collapsed").removeClass("expanded");
     			});

	 		} else {
	 		
	 			$(this).addClass("collapsed").removeClass("expanded");
	 			$(card).css("bottom", -1*ulHeight);
	 		}
	 	}); 

     	// $("body").click(function(e){

     	// 	var tab = $(".region-navigation #block-system-main-menu h2");
     	// 	var card = $(".region-navigation");
     	// 	if(tab.hasClass("expanded")){
     	// 		tab.addClass("collapsed").removeClass("expanded");
     	// 		$(card).css("bottom", -1*ulHeight + 3);
     	// 	}
     	// });

});

/* Funkcija koja sređuje language switcher na manjim ekranima */

function languageSwitcher() {
	var windowWidth = $(window).width();

	     if (windowWidth < 853) {
	     	$("li.en a").html("Eng");
	     	$("li.sh a").html("Srb");
	     	$("ul.language-switcher-locale-session").addClass("otpadnik");
	     	
	     } else {
	     	$("li.en a").html("English");
	     	$("li.sh a").html("Srpski");
	     	if ($("ul.language-switcher-locale-session").hasClass("otpadnik")) {
	     		$("ul.language-switcher-locale-session").removeClass("otpadnik");
	     	}
	     }
}
		
/* Contact form input type="text" padding-left,  */

function contactFormPadidng() {
	var labelWidth = $(".webform-component--info-group label").outerWidth();
	var windowWidth = $(window).width();
    var polje = $(".webform-component--info-group input");
    
    if (windowWidth > 450){
     $(polje).css("padding-left", labelWidth + 8);
    } else {
    	$(polje).css("padding-left", 2);
    }
}

/* Navicon */


 function navicon() {
  
  var bodyWidth = $("body").width(); /* merim sirinu viewporta */
  var windowHeight = $(window).height();
  var ulHeight = $(".region-navigation #block-system-main-menu ul").outerHeight(); /* merim visinu Ul elementa */
  var card = $(".region-navigation"); /* zamenjujem parent element sa varijablom */
  var tab = $(".region-navigation #block-system-main-menu h2"); 
  
  console.log(ulHeight);
  console.log($("#block-system-main-menu h2").css("display"));

  if ($("#block-system-main-menu h2").css("display") !== "none") {
  	
    tab.addClass("collapsed");  
    card.css("bottom", -1*ulHeight - 3); 
  
    } else {
    	card.css("bottom", 0);
  	}
  
}



/* Zakucavanje glavnog menija za vrh ekrana za ekrane preko 500px, prilikom scroll-a */

function stickyNav(){
	var scrollPosition = $(window).scrollTop();
	var windowHeight = $(window).height();
	var windowWidth = $(window).width();
	var objectHeight = $("#block-system-main-menu").outerHeight();
	
	if (scrollPosition >= windowHeight - objectHeight) {
		$("#block-system-main-menu").addClass("stickynav-active");
		
	} else {
		$("#block-system-main-menu").removeClass("stickynav-active");
	}
}