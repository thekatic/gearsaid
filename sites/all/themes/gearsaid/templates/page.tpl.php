<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

 
 <?php print render($page['navigation']); ?>     <!--  REGION "Navigation bar"  -->
 
 <header class="header" role="banner">
    <div class="layout-nav">   <!--  LAYOUT NAV  -->
      <div class="layout-center">
        
        

        <!-- Logo -->
        <?php if ($logo): ?>  
         <div class="logo-pic">
         <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo-image" />
         </div>
        <?php endif; ?> 
        <!-- END OF  Logo -->

        <!--  Site name and Slogan  -->
        <?php if ($site_name || $site_slogan): ?> 
          <div class="header__name-and-slogan">
            <?php if ($site_name): ?>
              <h1 class="header__site-name">
               <span class="header-title"><?php print $site_name; ?></span>
              </h1>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
              <div class="header__site-slogan header-subtitle"><?php print $site_slogan; ?></div>
            <?php endif; ?>
          </div>
        <?php endif; ?>                          
         <!-- END OF Site name and Slogan  -->

     <!--    <?php if ($secondary_menu): ?>
          <nav class="header__secondary-menu" role="navigation">
            <?php print theme('links__system_secondary_menu', array(
              'links' => $secondary_menu,
              'attributes' => array(
                'class' => array('links', 'inline', 'clearfix'),
              ),
              'heading' => array(
                'text' => $secondary_menu_heading,
                'level' => 'h2',
                'class' => array('visually-hidden'),
              ),
            )); ?>
          </nav>
        <?php endif; ?> -->

        <?php print render($page['header']); ?> <!-- Ovo je REGION Header, u njemu je samo language switcher (vidi structure -> blocks ) -->
      </div>
    </div>       
     <!--  END OF LAYOUT NAV  -->

     <!--  LAYOUT-CENTER MAIN-HEADING  -->
    <dir class="layout-center main-heading"> 
      <h1 class="heading">Everything you can imagine is real.</h1> <!-- glavni heading na strani  -->
      
    </dir>                                  
     <!--  END OF LAYOUT-CENTER MAIN-HEADING  -->
      

  </header>


  
  <!--  MAIN  - LAYOUT SWAP - SIDEBAR (ovde je sav content do footera)  -->
  <div class="layout-3col layout-swap">

    <div class="layout-center">
      <?php print render($page['about_us']); ?>
    </div>

    <?php print render($page['highlighted']); ?>    <!--  REGION "Highlighted" -->


    <!--  PAGE BODY  -->
<!--     <div class="layout-center"> -->
    
      <!--  Provera sidebar-a i dodeljivanje layout klasa  -->
      <?php
        // Render the sidebars to see if there's anything in them.
        $sidebar_first  = render($page['sidebar_first']);
        $sidebar_second = render($page['sidebar_second']);
        // Decide on layout classes by checking if sidebars have content.
        $content_class = 'layout-3col__full';
        $sidebar_first_class = $sidebar_second_class = '';
        if ($sidebar_first && $sidebar_second):
          $content_class = 'layout-3col__right-content';
          $sidebar_first_class = 'layout-3col__first-left-sidebar';
          $sidebar_second_class = 'layout-3col__second-left-sidebar';
        elseif ($sidebar_second):
          $content_class = 'layout-3col__left-content';
          $sidebar_second_class = 'layout-3col__right-sidebar';
        elseif ($sidebar_first):
          $content_class = 'layout-3col__right-content';
          $sidebar_first_class = 'layout-3col__left-sidebar';
        endif;
      ?>
      <!--  END OF Provera sidebar-a i dodeljivanje layout klasa  -->



      <!--  MAIN Glavni sadržaj  -->
      <main class="<?php print $content_class; ?>" role="main">
        
        <?php print $breadcrumb; ?>
        <a href="#skip-link" class="visually-hidden--focusable" id="main-content">Back to top</a>
        <?php print render($title_prefix); ?>
        <!-- Uklonjen page title ( vec imam iz teme ) -->
        <?php //if ($title): ?>
    <!--      <h1><?php // print $title; ?></h1> -->
        <?php // endif; ?>
        <?php print render($title_suffix); ?>
        <?php print $messages; ?>
        <?php print render($tabs); ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <?php print render($page['content']); ?>   <!--  REGION Content  -->
        <?php print $feed_icons; ?>
      </main>
      <!--  END OF MAIN Glavni sadržaj  -->



      <!--  LAYOUT-SWAP  -->
      <div class="layout-swap__top layout-3col__full">

        <a href="#skip-link" class="visually-hidden--focusable" id="main-menu" tabindex="-1">Back to top</a>
      </div>
       <!-- END OF LAYOUT-SWAP  -->


      <!--  SIDEBAR  -->
      <?php if ($sidebar_first): ?>
        <aside class="<?php print $sidebar_first_class; ?>" role="complementary">
          <?php print $sidebar_first; ?>   <!--  REGION First sidebar  -->
        </aside>
      <?php endif; ?>

      <?php if ($sidebar_second): ?>
        <aside class="<?php print $sidebar_second_class; ?>" role="complementary">
          <?php print $sidebar_second; ?> <!--  REGION Second sidebar  -->
        </aside>
      <?php endif; ?>
      <!-- END OF SIDEBAR  -->

<!--     </div> -->

    <!--  END OF  PAGE BODY  -->

  </div>
  <!-- END OF    MAIN  - LAYOUT SWAP - SIDEBAR (ovde je sav content do footera)  -->


  <?php print render($page['footer']); ?> <!--  REGION "Footer"  -->



<?php print render($page['bottom']); ?> <!--  REGION "Page bottom"  (region na samom dnu strane) -->
